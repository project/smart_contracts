<?php

/**
 * @file
 *  Produces forms, form elements, validation handlers and submission handlers
 *  for the smart_contracts module
 *
 * @author
 *  Timon Davis for Freelock Computing - 2012
 */

/**
 * Produces the contract signature form
 */
function smart_contracts_signature_form($node, $form_state) {

  $form = array();

  global $user;
  $uid = $user->uid;
  
  if ($contracts = $_SESSION['contracts_to_sign']) {

    $contract = $contracts->contracts[$contracts->current_index];
    $contract_nid = $contract['contract_nid'];
    $contract_node = node_load($contract_nid);
  }
  else {

    $contract_node = $node;
  }
  
  $theme_args = array();
  $theme_args['text'] = $contract_node->body;
  $theme_args['encapsulated'] = TRUE;

  $form['contract'] = array(
    '#type' => 'markup',
    '#value' => check_plain(theme('contract_text', $theme_args)),
  );

  $form['sign_here'] = array(
    '#type' => 'checkbox',
    '#return_value' => 'I agree',
    '#title' => t('I have read, and agree to, the terms and conditions listed on this page'),
    '#required' => TRUE,
  );

  if (!$contracts) {

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
  }

  $form['#submit'] = array();
  
  $form['#validate'][] = 'smart_contracts_validate_signature';
  $form['#submit'][] = 'smart_contracts_submit_signature';

  return $form;
}

/**
 * Implements hook_form_alter()
 * Attaches to the node edit form
 */
function smart_contracts_form_alter(&$form, $form_state, $form_id) {

  // We're going to attach a field group for all qualified candidate types here
  // To allow for association of contracts, sources and targets
  if (strpos($form_id, 'node_form') !== FALSE &&
       $form['#node']->nid) {

    // First, we'll collect our qualified nodes and see if this node type
    // gets to play.  We'll also collect our contract nodes and target
    // data arrays.
    $candidates = module_invoke_all('contract_source_data');
    $targets = module_invoke_all('contract_target_data');
    $contracts = _smart_contracts_get_contract_nodes();

    $current = smart_contracts_get_most_recent_contract_for_source($form['#node']->type,
      $form['#node']->nid);

    if ($current) {
      $default_contract = $current['contract_nid'];
    }

    // Extract the machine name for each entry from the results of the hook invoke
    $candidate_names = array();
    foreach ($candidates as $name => $candidate) {
      $candidate_names[] = $name;
    }
  
    $target_names = array();
    foreach ($targets as $name => $target) {
      $target_names[] = $name;
    }

    // Collect the forms node type so we have something to compare against
    $item_type = $form['#node']->type;

    if (in_array($item_type, $candidate_names)) {

      // Add in our formlet as a fieldset
      $form['smart_contracts'] = array(
        '#type' => 'fieldset',
        '#title' => t('Smart Contracts'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#weight' => 10,
      );
  
      // Enable Radio Button (NOT USED YET)
      $form['smart_contracts']['sc_enable'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable Contracts on This Node'),
        '#return_value' => 'smart_contracts enabled',
      );

      // CONTRACT Fieldset
      $form['smart_contracts']['contract'] = array(
        '#type' => 'fieldset',
      );
  
      // CONTRACT SELECTION dropbox
      $form['smart_contracts']['contract']['use_contract'] = array(
        '#type' => 'select',
        '#options' => $contracts,
        '#title' => t('Select a Contract'),
      );

      if ($current) {

        $form['smart_contracts']['contract']['use_contract']['#default_value'] =
          $default_contract;
      }
  
      // CONTRACT TARGET AREAS fieldset
      $form['smart_contracts']['contract']['targets'] = array(
        '#type' => 'fieldset',
        '#title' => t('Targets'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
  
      // Store the names of the targets
      $form['smart_contracts']['contract']['targets']['target_names'] = array(
        '#type' => 'hidden',
        '#value' => implode(',', $target_names),
      );
  
      // Cycle through each target and create a mini-menu
      foreach ($targets as $machine_name => $target) {
  
        // TARGET's own fieldset
        $form['smart_contracts']['contract']['targets'][$machine_name . '_grp'] = array(
          '#type' => 'fieldset',
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#title' => check_plain($target['human_name']), 
        );
  
        // TARGET's own checkbox
        $form['smart_contracts']['contract']['targets'][$machine_name . '_grp'][$machine_name . '_cbx'] = array(
  
         '#type' => 'checkbox',
         '#return_value' => $machine_name,
         '#title' => check_plain('Target ' . $target['human_name']),
        );
  
        /*
        Feature is depricated from current version - may return in later release
        // TARGET IDENTIFIER textfield.  Only displays if multiple targets are possible
        if ($target['multi_value']) {
          $form['smart_contracts']['contract']['targets'][$machine_name . '_grp'][$machine_name . '_txt'] = array(
      
            '#type' => 'textfield',
            '#cols' => 10,
            '#description' => 'Enter the ID of each ' . $target['human_name']
              .' you wish to target, separated by semicolons (;)',
          );
        }*/
      }
  
      // Then the standard stuff, statically generated
      $form['smart_contracts']['contract']['options'] = array(
        '#type' => 'fieldset',
        '#title' => t('Options'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
  
      // REQUIRE SIGNATURE checkbox
      $form['smart_contracts']['contract']['options']['signature_required_once'] = array(
        '#type' => 'checkbox',
        '#title' => t('Require signure of this contract'),
        '#return_value' => 'signature_required_once',
      );
   
      // REQUIRE SIGNATURE ON CONTRACT UPDATE checkbox
      $form['smart_contracts']['contract']['options']['signature_required_always'] = array(
        '#type' => 'checkbox',
        '#title' => t('Require signature of this contract on each new revision'),
        '#return_value' => 'signature_required_always'
      );
  
      // SET EXPIRATION DATE checkbox
      $form['smart_contracts']['contract']['options']['contract_expiration_date'] = array(
        '#type' => 'textfield',
        '#title' => t('Expiration Date'),
        '#description' => t('For an absolute expiration date, enter expiration date in mm/dd/yyyy format.'.
          'For a relative date, enter a UNIX epoch relative date string (i.e. \'+30 days\') ' .
          'Enter \'-1\' for no expiration date.'),
  
      );

      // Look for any enabled contracts with this node as a source.
      // Auto-check the checkbox if we have any
      $contract = smart_contracts_get_contracts_for_source(
        $form['#node']->type, $form['#node']->nid);

      // Add in our own specialty handlers for this form
      $form['#validate'][] = 'smart_contracts_form_validate';
      $form['#submit'][] = 'smart_contracts_form_submit';

      // If there is no contracts to speak of, there's nothing further
      // to configure on the form.
      if (!$contract) {

        return;
      }
  
      if ($contract['enabled']) {
  
        $form['smart_contracts']['sc_enable']['#default_value'] =
        $form['smart_contracts']['sc_enable']['#return_value'];
      }

      if ($contract['signature_required']) {

        $form['smart_contracts']['contract']['options']['signature_required_once']['#default_value'] =
        $form['smart_contracts']['contract']['options']['signature_required_once']['#return_value'];
      }

      if ($contract['signature_required_on_revision']) {

        $form['smart_contracts']['contract']['options']['signature_required_always']['#default_value'] =
        $form['smart_contracts']['contract']['options']['signature_required_always']['#return_value'];
      }

      if ($contract['expiration_date']) {

        $form['smart_contracts']['contract']['options']['contract_expiration_date']['#default_value'] =
        ($contract['expiration_date'] == '-1') ? '-1' : date('m/d/Y', $contract['expiration_date']);
 
        if ($form['smart_contracts']['contract']['options']['contract_expiration_date']['#default_value'] == '12/31/1969') {

          $form['smart_contracts']['contract']['options']['contract_expiration_date']['#default_value'] =
            $contract['expiration_date'];
        }
      }
    }
  


    $source_contract = smart_contracts_get_contracts_for_source(
      $item_type, $form['#node']->nid);

    if ($source_contract) {

      $type_contract = smart_contracts_get_entity_contract_with_targets($source_contract['contract_id'],
        $target_names);
      
      if ($type_contract) {
  
        foreach ($type_contract as $contract) { 
    
          if ($contract['enabled']) {
            $target_type = $contract['target_type'];
            $form['smart_contracts']['contract']['targets'][$target_type . '_grp'][$target_type . '_cbx']['#default_value'] =
              $form['smart_contracts']['contract']['targets'][$target_type .'_grp'][$target_type . '_cbx']['#return_value'];
          }
        }
      }
    }
  }
}


/**
 * Verify conditions on the contract signature page
 */
function smart_contracts_validate_signature($form, &$form_state) {

  if (!$form_state['values']['sign_here']) {

    form_set_error('sign_here', t('You must agree to terms and conditions to continue.'));
  }
}

/**
 * Validate smart_contracts node form (a part of the node edit form)
 */
function smart_contracts_form_validate($form, &$form_state) {

  // If the enable box wasn't checked, then we don't have much validation to
  // do about it.  Return to flow of control.
  if (!$form_state['values']['sc_enable']) {
    return;
  }

  // The box is checked.  We need to make sure everything matches up okay.
  $target_names = explode(',', $form_state['values']['target_names']);
 
  $targets = array();

  // Collect data about activated nodes
  foreach ($target_names as $target_name) {
  
    // Collect Target Data for each checked target type.

    // If the box for this target has been checked
    if ($form_state['values'][$target_name . '_cbx'] != FALSE) {

      // We'll add this target's data to the target data array for validation
      $targets[$target_name] =
        module_invoke_all('contract_target_data', $target_name);
    }

    if (!empty($targets)) { 
      if (array_key_exists('validate', $targets[$target_name])) {
  
        // Call the validation handler to check user input on this type
        $result = call_user_func($targets[$target_name]['validate'],
          $targets[$target_name]);
  
        if ($result) {
  
          form_set_error($target_name . '_txt', $result);
        }
      }
    }
  }

  // Check standardized components for conformity to necessary norms.
  $proposed_date_string = $form_state['values']['contract_expiration_date'];

  $proposed_date = strtotime($proposed_date_string);
  $todays_date = time();

  if ($proposed_date == 0 && $form_state['values']['contract_expiration_date'] !=
    '') {

    form_set_error('contract_expiration_date', t('The contract expiration '  .
      'date is invalid'));
  }

  if ($proposed_date < $todays_date &&
    ($proposed_date_string != '-1' && $proposed_date_string != '')) {

    form_set_error('contract_expiration_date', t('The contract expiration ' .
      'date must be later than today'));
  }

}

/**
 * Handle submissions from the contract signature page
 */
function smart_contracts_submit_signature($form, &$form_state) {

  global $user;

  $entity_contract_id = $form['#parameters'][2];
  $contract = smart_contracts_get_entity_contract($entity_contract_id);
  $contract_node = node_load($contract['contract_nid']);

  $signature_data = module_invoke_all('contract_collect_signature_data');
  $source_data = module_invoke_all('contract_source_data');

  // Create database write object
  $object = new stdClass();

  $object->user_id   = $user->uid;
  $object->contract_id = $entity_contract_id;
  $object->current_contract_vid = $contract_node->vid;
  $object->date_signed = time();
  $object->expiry_date = ($contract['expiration_date'] == '-1' ) ?
    -1 : strtotime($contract['expiration_date']);
  $object->signature_data = serialize($signature_data);

  call_user_func_array($source_data[$contract['source_type']]['signature_submit'], array($object));

  $type_data = module_invoke_all('contract_source_data', $contract['source_type']);
  drupal_goto($type_data[$contract['source_type']]['return_path']);
}

/**
 * Handle form submission for node edit form
 */
function smart_contracts_form_submit($form, &$form_state) {

  // Acquire the values collection from form state values
  $values = $form_state['values'];

  // Pick out the pieces of informaiton we will use from form and form_state
  $target_names  = explode(',', $values['target_names']);
  $enabled       = $values['sc_enable'];
  $source_node   = $form['#node'];

  $signature_required_once = $values['signature_required_once'];
  $signature_required_always = $values['signature_required_always'];
  $contract_expiration_date = $values['contract_expiration_date'];

  // Use above data to collect derived information
  $source_id = $source_node->nid;
  $source_type = $source_node->type;
  $contract     = smart_contracts_get_contracts_for_source(
    $source_type, $source_id);

  
  // Now the fun begins.  We'll start by testing whether or not this
  // contract is enabled.

  // If the contracts are checked as enabled, we'll work from there.
  try {

    if ($enabled) {

      // Load up the contract node that was defined on the user form.
      $contract_node = node_load($values['use_contract']);

      // There needs to be a contract node present if this is going to work.
      if (!$contract_node) {

        throw new Exception('Contract node not found in '.
          'smart_contracts_form_submit()', 5000);
      }

      // If a contract already exists, we'll consider wether or not the DB
      // record needs revision to match new user preferences. If it does,
      // we'll make the updates, otherwise we'll leave it alone.
      if ($contract) {
  
        $needs_update = FALSE;  // Flag indiciating whter or not an update is
                                // necessary
        $needs_new_entry = FALSE;  // Flag indicating that a new row is required
                                   // to reflect the changes indicated by user.

        // Test to see if we need a new entry first
        $proposed_contract['source_id'] = $source_id;
        $proposed_contract['contract_nid']       = $contract_node->nid;
        $proposed_contract['contract_vid']       = $contract_node->vid;

        $needs_new_entry =
          _smart_contracts_needs_new_entry($proposed_contract, // TODO IMPLEMENT
            $contract);

        // If the record doesn't need a new entry, we'll see if it needs an
        // update.
        $proposed_contract['expiration_date'] = $contract_expiration_date;
        $proposed_contract['signature_required'] = $signature_required_once;
        $proposed_contract['signature_required_on_revision'] =
          $signature_required_always;
        $proposed_contract['enabled'] = $enabled;

        $needs_update =
          _smart_contracts_needs_update($proposed_contract, $contract); // TODO IMPLEMENT

        // Now we know where we stand.  If we need a new update, that's an
        // exclusive operation that will forgo any other scenario.
        if ($needs_new_entry) {

          smart_contracts_insert_alternate_contract_record($proposed_contract,
            $contract); // TODO IMPLEMENT
        }

        // Next we'll see if we need to update anything on the contract if we
        // didn't need to make a new version
        elseif ($needs_update) {

           smart_contracts_update_contract_entry($proposed_contract, $contract); // TODO IMPLEMENT
        }

        // If neither of these conditions applied, then no action is otherwise
        // required.  We're done with this particular scenario.
      }

      // In this scenario, there was no prexisting contract.  Easy-peasie.
      // We'll just write up a new one.
      else {

        // Build the new contract data
        smart_contracts_node_source_insert_new_contract_record($values,
          $contract_node, $source_node);
        $contract = smart_contracts_get_contracts_for_source(
          $source_type, $source_id);
      }

      // Now we have the contract record management squared away.  Time to
      // look at the actual targets now.  The first step is to collect which
      // targets were enabled and which were not.
      if (!empty($target_names)) {

        // We should have the contract variable filled in by this point.
        // If we don't, let's throw an exception.
        if (!$contract) {

          throw new Exception('The $contract component needs a value', 5003);
        }

        $enabled_targets = array(); // Checked targets
        $disabled_targets = array(); // Un-checked targets

        foreach ($target_names as $target_name) {

          // If the box for this target has been checked
          if ($values[$target_name . '_cbx'] != FALSE && $values['sc_enable']) {

            $enabled_targets[$target_name] =
              module_invoke_all('contract_target_data', $target_name);
          }
          else { // The box was unchecked for this target

            $disabled_targets[$target_name] =
              module_invoke_all('contract_target_data', $target_name);
          }
        }

        //Now that we know what we need to do with each target, we'll
        //invoke the handlers for both the enabled targets and the disabled
        //targets

        // Handle enabled targets
        if (!empty($enabled_targets)) {

          foreach ($enabled_targets as $name => $target) {

            call_user_func($target[$name]['enable_target'], $target[$name], $contract);
          }
        }

        //Handle disabled targets
        if (!empty($disabled_targets)) {

          foreach ($disabled_targets as $name => $target) {

            call_user_func($target[$name]['disable_target'], $target[$name], $contract);
          }
        }
      }
    }

    else { 
      // The contract 'enabled' box is unchecked.  We need to ensure
      // That the contract is disabled and that the targets are cleared
      // away

      if ($contract) {

        //There is an existing contract in the database. We simply need it
        // to be disabled.
        smart_contracts_disable_contract($contract);
      }

      // Regardless of existing contract, we need to disable all targets.
      if (!empty($target_names)) {

        // Loop through all names, collect the metadata and disable the contract
        foreach ($target_names as $target_name) {

          $target = module_invoke_all('contract_target_data', $target_name);
          call_user_func($target[$target_name]['disable_target'], $target, $contract);
        }
      }
    }
  }

  catch (Exception $ex) {

    watchdog('smart_contracts', $ex->getMessage(), array('error'));
  }
}

/**
 * Used to return contract revisions list in content form
 */
function _smart_contracts_get_revisions($contract_id) {

  $sql = "
    SELECT *
    FROM {". CONTRACT_TABLE ."}
    WHERE contract_id = %d";

  $result = db_query($sql, $contract_id);

  if ($record = db_fetch_array($result)) {

    $sql = "
      SELECT contract_vid, contract_nid
      FROM {". CONTRACT_TABLE ."}
      WHERE contract_nid = %d
      AND contract_vid < %d";

    $result = db_query($sql, $record['contract_nid'], $record['contract_vid']);

    $items = array();
    $theme_items = array();

    while ($record = db_fetch_array($result)) {

       $items[] = node_load($record['contract_nid'], $record['contract_vid']);
    }

    if ($items) {

      foreach ($items as $item) {

        $theme_items[] = l(date('m/d/Y H:i', $item->revision_timestamp), 'node/' . $item->nid .
          '/revisions/' . $item->vid . '/view');

      }

      $content = '<p class="previous-revisions">Previous Revisions';
      $content .= theme('item_list', $theme_items);
      $content .= '</p>';

      return $content;
    }

    else return;
  }

  return;
}

