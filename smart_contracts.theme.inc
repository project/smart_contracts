<?php
/**
 * @file
 *  Provides theme handling for smart_contracts module
 *
 * @author
 *  Timon Davis for Freelock Computing - 2012
 */
function theme_contract_text($arguments) {

  drupal_add_css(drupal_get_path('module', 'smart_contracts') .
    '/smart_contracts-contract.css', 'module');

  $text = $arguments['text'];
  $encapsulated = $arguments['encapsulated'];

  $content = '';

  //$content .= _smart_contracts_get_revisions(arg(1));

  if ($encapsulated) {

    $content .= '<div id="contract-encapsulated">' . $text . '</div>';
  }

  else {

    $content .= '<div id="contract">' . $text . '</div>';
  }
  return $content;
}

function theme_contract_target_table($options) {

  $class = 'form-checkboxes';
  if (isset($element['#attributes']['class'])) {
    $class .= ' '. $element['#attributes']['class'];
  }
  $element['#children'] = '<div class="'. $class .'">'. (!empty($element['#children']) ? $element['#children'] : '') .'</div>';
  if ($element['#title'] || $element['#description']) {
    unset($element['#id']);
    return theme('form_element', $element, $element['#children']);
  }
  else {
    return $element['#children'];
  }

}
