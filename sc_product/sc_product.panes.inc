<?php
/**
 * @file
 *  Checkout pane handlers.  The methods here are used to relay contract
 *  information between the site's administrators and users.  They display
 *  the contract and collect signatures
 *
 * @author
 *  Timon Davis for Freelock Computing - 2012
 */

/**
 * Handler for cart checkout pane
 */
function sc_product_contract_checkout_pane($op = 'view', $arg1, $arg2) {

  // Pull in user info
  global $user;
  $uid = $user->uid;

  switch ($op) {

    // This view will be called for each pane that contains a contract (there
    // may be multiple if multiple products need a signature).
    case ('view'): {

      // Get the contracts object from the session (put into place by the
      // hook_cart_pane method in the sc_product module)
      $contracts = $_SESSION['contracts_to_sign'];

      // The object keeps track of the current contract index.  We're going
      // to pull out the contract at the current index and collect some basic
      // information about it.
      $contract = $contracts->contracts[$contracts->current_index];
      $contract_nid = $contract['contract_nid'];
      $contract_node = node_load($contract_nid);

      // Pull in the smart contracts form array (raw)
      $content = smart_contracts_signature_form(NULL,
        array('contract_text' => $contract_node->body));

      // Add a little something to tell the handler about the contract data
      // which is being signed.
      $content['contract_data'] = array(
        '#type' => 'hidden',
        '#value' => serialize($contract),
      );
       
      // Other requisite information about the behavior and contents of the pane
      // When we return the pane, it should have the contents of the contract
      // And a checkbox area
      $return['description'] = $contract_node->title;
      $return['contents'] = $content;
      $return['next_button'] = FALSE;

      // Decrement the current index
      $contracts->current_index--;

      // Test the current index.  If its less than 0 we are done.  We'll
      // either erase the session variable if we don't need it anymore,
      // or we'll assign the adjusted contracts object to the session.
      if ($contracts->current_index < 0) {

        unset($_SESSION['contracts_to_sign']);
      }
      else {

        $_SESSION['contracts_to_sign'] = $contracts;
      }

      break;
    }

    // Much like the view scenario, each process is handled individually for
    // what may be multiple cart panes.
    case ('process'): {

      // Make $arg2 a little more readable (these are form_state values,
      // essentially).  We'll also extract the results of the signature
      // checkbox test.
      $values = $arg2;
      $sign_here = $values['sign_here'];

      // If the contract wasn't signed, hold the happiness and report an error.
      if (!$sign_here) {

        drupal_set_message(
          'You must agree to all Terms & Conditions to continue', 'error');

        // Returning false cancels the cart checkout submission
        return FALSE;
      }
      else {

        // The checkbox was signed.  We'll submit the signature to the database.
        global $user;
        $account = user_load($user->uid);

        // Collect the contract data from the form
        $contract_data = unserialize($values['contract_data']);

        // Collect data about the source which prompted the trigger
        $source_data = module_invoke_all('contract_source_data', 'product');

        // Build signature object
        $signature = new stdClass();

        $signature->user_id = $account->uid;
        $signature->contract_id = $contract_data['contract_id'];
        $signature->current_contract_vid = $contract_data['contract_vid'];
        $signature->date_signed = time();
        $signature->expiry_date = strtotime($contract_data['expiration_date']);

        // Call the submission handler on the sc_product's defined source
        // data model
        call_user_func_array($source_data['product']['signature_submit'], array($signature));
      }
    }
  }

  return $return;
}