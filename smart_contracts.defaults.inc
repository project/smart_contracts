<?php
/**
 * @file
 *  Provides default handlers for source and target types
 *
 * @author
 *  Timon Davis for Freelock Computing - 2012
 */

/**
 * Default
 *
 * Handler for Source Contract Managent Subform (shows up on node edit page)
 * Used for processing requests to add / activate contract associations
 * on a source for the given target type
 *
 * @param $target_data
 *  Contains target data 
 * @param $contract
 *  Data about the current contract
 */
function smart_contracts_default_enable_target($target_data, $contract) {

  // First, query to see if this source has this target enabled already.  If
  // so, we don't need to execute any further action
  $target_enabled = FALSE;

  $sql = "
    SELECT *
    FROM {" . TARGET_TABLE . "}
    WHERE target_id = '%s' 
      AND contract_id = '%s'
      AND target_type = '%s'";

  $result = db_query($sql, '-1', $contract['contract_id'],
    $target_data['machine_name']);

  // If we get a return from this, our work is done.
  if ($record = db_fetch_array($result)) {

    return;
  }

  // If we've hit this point, we need to create the new entry
  $new_record = new stdClass(); //<-- container for new db record

  $new_record->target_id = '-1'; // Indicates lack of multiplicity.
  $new_record->contract_id = $contract['contract_id'];
  $new_record->target_type = $target_data['machine_name'];

  // Set table name
  $table_name = TARGET_TABLE;

  // Commit record to database
  $success = drupal_write_record($table_name, $new_record);

  // Catch errors
  if (!$success) {

    throw new Exception('Could not add new record to ' . TARGET_TABLE .
      ' table', 5001);
  }
}

/**
 * Default
 *
 * Handler for Source Contract Management Subform when a target has been
 * disabled.
 *
 * @param $target_data
 *  Contains target data
 * @param $contract
 *  Contains data about the contract
 */
function smart_contracts_default_disable_target($target_data,
  $contract = FALSE) {

  // First, query to see if this source has this target enabled already.  If
  // so, we don't need to execute any further action
  $target_enabled = FALSE;

  $sql = "
    SELECT *
    FROM {" . TARGET_TABLE . "}
    WHERE target_id = '%s' 
      AND contract_id = '%s'
      AND target_type = '%s'";

  $result = db_query($sql, '-1', $contract['contract_id'],
    $target_data['machine_name']);

  // If we get a return from this, we need to remove the record
  if ($record = db_fetch_array($result)) {

    $sql =  "
      DELETE
      FROM {" . TARGET_TABLE . "}
      WHERE target_tag = %d";

    // Execute query
    $result = db_query($sql, $record['target_tag']);
  }

  // Nothing more to do.  If there was no record we had no work to do.
  // Go Team!
}