<?php

/**
 * @file
 *  Handles page calls, return information and granting access to those
 *  with proper authorization.
 *
 * @author
 *  Timon Davis for Freelock Computing - 2012
 */

/**
 * Handler for the contract page
 */
function smart_contracts_signature_page() {

  $contract_entity_id = arg(1);

  $contract = smart_contracts_get_entity_contract($contract_entity_id);
  $contract_node = node_load($contract['contract_nid'], $contract['contract_vid']);
   
  drupal_set_title($contract_node->title);

  $theme_args = array();
  $theme_args['text'] = $contract_node->body;
  $theme_args['encapsulated'] = TRUE;

  $content = '';

  $content .= theme('contract_text', $theme_args);
  $content .= drupal_get_form('smart_contracts_signature_form', $contract_entity_id);
   
  return $content;
}

function smart_contracts_signature_access() {

  global $user;

  if (in_array('authenticated user', $user->roles)) {

    return TRUE;
  }

  return FALSE;
}