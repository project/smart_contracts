<?php
/**
 * @file
 *  Allows contracts to be associated with content types,
 *  as well as enforced and recorded according to specification using
 *  hooks and handlers
 *
 * @author
 *  Timon Davis for Freelock Computing - 2012
 */
require_once('smart_contracts.forms.inc');
require_once('smart_contracts.theme.inc');
require_once('smart_contracts.pages.inc');
require_once('smart_contracts.defaults.inc');

define(CONTRACT_TABLE, 'smart_contracts_contract_instance');
define(TARGET_TABLE, 'smart_contracts_target');
define(SIGNATURE_TABLE, 'smart_contracts_signature');

/**
 * Invokes hook_block()
 */
function smart_contracts_block($op = 'list', $delta = 0, $edit = array()) {

  $return = array();

  switch ($op) {

    case ('list'): {

      // 0  This block displays contract text of a contract-associated node
      // on any page associated with the node.
      $return[] = array(
        'info' => t('Contract Text'),
        'cache' => BLOCK_NO_CACHE,
      );

      break;
    }

    case ('view'): {

      switch ($delta) {

        case ('0'): {

          // Get the router item and determine if we are on a node page.
          $router_item = menu_get_item();

          if ($router_item['map'][0] == 'node') {

            // Grab the node owning the page we are on
            $node = $router_item['map'][1];

            // Get the contarct associated with this node page.
            $contract = smart_contracts_get_most_recent_contract_for_source($node->type,
              $node->nid);

            // If there is no contract here, do not display the node and
            // return instead.
            if (!$contract) {

              return $return;
            }

            // Load the contract node belonging to this node
            $contract_node = node_load($contract['contract_nid']);

            // Prepare the show the display of the contract text block.
            $variables = array();
            $variables['text'] = $contract_node->body;

            $return['subject'] = check_plain($contract_node->title);
            $return['content'] = theme('contract_text', $variables);
          }
          break;
        }
      }

      break;
    }
  }

  return $return;
}

/**
 * Invokes hook_cron()
 */
function smart_contracts_cron() {

  // Go through and collect all expired signatures
  $expired_signatures = smart_contracts_get_expired_signatures();

  foreach ($expired_signatures as $signature) {

    // Call submodules to 
    module_invoke_all('process_expired_signature', $signature);
  }
}

/**
 * Invokes hook_menu()
 */
function smart_contracts_menu() {

  $items = array();
  
  $items['terms-and-conditions/%'] = array(
    'page callback' => 'smart_contracts_signature_page',
    'page arguments' => array(1),
    'access callback'   => 'smart_contracts_signature_access',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Invokes hook_theme()
 */
function smart_contracts_theme() {

  $items = array();
  
  $items['contract_target_table'] = array(
    'arguments' => '');
  $items['contract_text'] = array(
    'arguments' => array(
      'text' => '',
      'encapsulated' => FALSE,
     ),
  );

  return $items;
}

/**
 * Invokes hook_nodeapi
 */
function smart_contracts_nodeapi(&$node, $op) {

  // This routine handles contract revisins
  if ($node->type == 'smart_contract') {
    switch ($op) {

      // Check to see if this is a node revision (the node has a new vid)
      case ('update'): {

        $update = TRUE;

        $sql = "
          SELECT MAX(contract_vid)
          FROM {" . CONTRACT_TABLE . "}
          WHERE contract_nid = %d";

        if ($record = db_fetch_array(db_query($sql, $node->nid))) {

          if ($record['MAX(contract_vid)'] >= $node->vid) {

            $update = FALSE;
          }
        }

        // If the node has a new vid, we'll proceed with the update routine.
        if ($update) {

          module_invoke_all('pre_update_contract', $node);

          $node_id = $node->nid;
          $vid = $node->vid;
    
          // Collect a list of contracts to update
          $contracts_to_update = _smart_contracts_collect_outdated_contracts_records($node_id);
    
          // Collect a count of contracts to update
          $num_contracts = count($contracts_to_update);
    
          // Define the ceiling of contract IDs
          $max_id = $contracts_to_update[$num_contracts -1];

          // Define the latest contract
          $update_array = array($max_id);
    
          // Send latest contract and relevant information to the upversion
          // method.  This method can handle many contracts, we're sending only
          // one.
          _smart_contracts_upversion_contracts($update_array, $node_id, $vid);
        }
      }

      break;
    }
  }
}

/**
 * This method is used by submodules as an easy way to identify if a user has
 * any contracts that needs attention while tripping a given trigger
 *
 * @param $uid
 *  The uid if the User
 * @param $target_type
 *  The machine name of the target
 * @param $target_id (optional)
 *  For multi-targets, the string identifier for the target instance
 * @param $context
 *  Context is passed along to the target data handler, the method defined
 *  in the contract_target_data() hook
 *
 * @return
 *  Returns contract ids which need attention from the end user
 */
function smart_contracts_needs_signature($uid, $target_type, $target_id = NULL, $context = NULL) {

  // Pull up all target data for the given data type
  $target_data_set = module_invoke_all('contract_target_data', $target_type);

  // Create a container to collect results of the following inquiry
  $result = array();

  // For all of the target datas collected
  foreach ($target_data_set as $target_data) {

    // Call the target's handler to report on which contract to enforce at target
    $this_result = call_user_func($target_data['handler'], $uid, $target_id, $context);

    if ($this_result) {

      $result = array_merge($this_result, $result);
    }
  }

  return $result;
}

/**
 * Returns the contract record for the given contract id
 *
 * @param contract_id
 *  The primary key (contract_id) field on the contracts table
 */
function smart_contracts_get_entity_contract($contract_id) {

  $sql = "
    SELECT *
    FROM {" . CONTRACT_TABLE ."}
    WHERE contract_id = %s";

  $result = db_query($sql, $contract_id);

  while ($record = db_fetch_array($result)) {

    return $record;
  }

  return FALSE;
}

/**
 * Collect entity contract record, a full row for each of the defined types
 * associated with the record as well
 *
 * @param $contract_id
 *  The contract_id PK of the given contract record
 * @param $types
 *  An array of strings, containing machine names of targets
 *
 * @return
 *  An array of contract rows, all bearing the same contract and joined with
 *  target data for that contract (if applicable)
 */
function smart_contracts_get_entity_contract_with_targets($contract_id, $types) {

  $sql = "
    SELECT *
    FROM {". CONTRACT_TABLE ."} AS ec
      LEFT JOIN {" . TARGET_TABLE ."} AS ct
      ON ec.contract_id = ct.contract_id
    WHERE ec.contract_id = %d
      AND ct.target_type IN (
  ";

  $revs = 0;
  $max_revs = count($types) - 1;
  foreach ($types as $type) {

    $sql .= "'%s'";

    if ($revs < $max_revs) {

      $sql .= ',';
    }
    else {

      $sql .= ')';
    }

    $revs++;
    
  }

  $args = array();

  $args[] = $contract_id;
  foreach ($types as $type) {

    $args[] = $type;
  }

  $return = array();
  $result = db_query($sql, $args);

  while ($record = db_fetch_array($result)) {

    $return[] = $record;
  }
  return $return;
}

/**
 * Collects the NID of all contract nodes
 *
 * @return
 *  Returns an key => value array, where key = nid and value = node title
 */
function _smart_contracts_get_contract_nodes() {

  $sql = "
    SELECT nid, title
    FROM {node}
    WHERE type = '%s'";

  $result = db_query($sql, 'smart_contract');

  $return = array();

  while ($record = db_fetch_array($result)) {

    $return[$record['nid']] = $record['title'];
  }

  return $return;
}

/**
 * Collect all of the contracts associated with a given target
 *
 * @param $target_type
 *  The machine name of the desired target
 * @param $target_id_array (optional)
 *  Allows for definition of instances on multi-targets
 *
 * @return
 *  Returns an array of contrats associated with the defined target.
 */
function smart_contracts_get_contracts_for_target($target_type, $target_id_array = NULL) {

  $sql = "
    SELECT *
    FROM {" . TARGET_TABLE ."} AS ct
      LEFT JOIN {". CONTRACT_TABLE ."} as ec
      ON ec.contract_id = ct.contract_id
    WHERE ct.target_type = '%s'";

  if ($target_id_array) {

    $sql .= "
      AND ct.target_id IN (%s)";
 
    $result = db_query($sql, $target_type, implode(',', $target_id_array));
  }
  else {

    $result = db_query($sql, $target_type);
  }

  $return = array();

  if ($result) {

    while ($record = db_fetch_array($result)) {

      $return[$record['contract_id']] = $record;
    }
  }

  return $return;
}

/**
 * Determine if a signature has been obtained from the defined user
 * for the defined contract.  Applies only to the contract, not contract
 * updates
 *
 * @param $uid
 *  The UID of the user
 * @param $contract_id
 *  The ID of the contract being queried against
 *
 * @return
 *  TRUE if a signature has been obtained for this contract, FALSE if not.
 */
function smart_contracts_is_signature_obtained($uid, $contract_id) {

  $contract = smart_contracts_get_entity_contract($contract_id);

  // Collect backhistory of this contract id
  $sql = "
    SELECT contract_id
    FROM {". CONTRACT_TABLE ."}
    WHERE source_id = '%s'
    AND source_type = '%s'
    AND contract_nid = %d";

  $result = db_query($sql, $contract['source_id'], $contract['source_type'],
    $contract['contract_nid']);

  while ($record = db_fetch_array($result)) {

    $sql = "
      SELECT date_signed
      FROM {". SIGNATURE_TABLE ."}
      WHERE contract_id = %d";

    $inner_result = db_query($sql, $record['contract_id']);

    while ($inner_record = db_fetch_array($inner_result)) {

      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Collect all of the target data surrounding a given contract
 *
 * @param $contract_id
 *  The ID of the contract in question
 *
 * @return
 *  Returns an array of Target records
 */
function smart_contracts_get_targets_for_contract($contract_id) {

  $sql = "
    SELECT *
    FROM {" . TARGET_TABLE ."}
    WHERE contract_id = %d";

  $result = db_query($sql, $contract_id);

  $return = array();

  while ($record = db_fetch_array($result)) {

    $return[] = $record;
  }

  if (empty($return)) {

    return FALSE;
  }

  return $return;
}

/**
 * Reports on whether or not the lastest version of a contract has been signed
 *
 * @param $uid
 *  The UID of the user in question
 * @param $contract_id
 *  The contract against which to check signature history.
 *
 * Returns TRUE if the latest version has been signed.  FALSE if not.
 */
function smart_contracts_is_signature_current($uid, $contract_id) {

  $sql = "
    SELECT *
    FROM {". SIGNATURE_TABLE ."} AS cs
      LEFT JOIN {". CONTRACT_TABLE ."} AS ec
      ON cs.contract_id = ec.contract_id
    WHERE user_id = %s
      AND cs.contract_id = %s";

  $result = db_query($sql, $uid, $contract_id);

  $max_signed_version = 0;

  while ($record = db_fetch_array($result)) {

    $current_version = $record['contract_vid'];

    if ($record['current_contract_vid'] > $max_signed_version) {
   
      $max_signed_version = $record['current_contract_vid'];
    }
  }

  if ($current_version > $max_signed_version || !$current_version) {

    return FALSE;
  }

  return TRUE;
}

/**
 * Used by the contract version updating routine, this method collects
 * all of the records which use this contract node.
 *
 * @param $node_id
 *  The nid of the contract which is being updated
 *
 * @return
 *  Returns an array of contract records which will need to be updated.
 */
function _smart_contracts_collect_outdated_contracts_records($node_id) {

  $sql = "
    SELECT contract_id
    FROM {". CONTRACT_TABLE ."}
    WHERE contract_nid = %d";

  $result = db_query($sql, $node_id);

  $return = array();

  while ($record = db_fetch_array($result)) {

    $return[] = $record['contract_id'];
  }

  if (array_key_exists(0, $return)) {

    return $return;
  }

  return FALSE;
}

/**
 * Handle updated contracts, creating a new record in the database which
 * implicitly requires all signees to sign again if they need to keep current.
 *
 * @param $contract_ids
 *  ID #s belonging to given contract history
 * @param $contract_nid
 *  The node ID of the actual contract node
 * @param $contract_vid
 *  The most current version ID of the actual contract node.
 */
function _smart_contracts_upversion_contracts($contract_ids, $contract_nid, $contract_vid) {

  // Cycle through each contract ID
  foreach ($contract_ids as $id) {

    // Load the contract in question
    $contract = smart_contracts_get_entity_contract($id);

    // Create a new database reporting object, which will be used to create
    // the new contract record on the contract table.
    $object = new stdClass();
    
    $object->source_id = $contract['source_id'];
    $object->contract_nid = $contract_nid;
    $object->contract_vid = $contract_vid;
    $object->implementation_date = $contract['implementation_date'];
    $object->signature_required = $contract['signature_required'];
    $object->signature_required_on_revision = $contract['signature_required'];
    $object->expiration_date = $contract['expiration_date'];
    $object->source_type = $contract['source_type'];
    $object->enabled = $contract['enabled'];

    // Write the new record to the database.
    drupal_write_record(CONTRACT_TABLE, $object);

    // @TODO:  Delegate to submodules
    // Create new target entries for the new contract record
    $targets = smart_contracts_get_targets_for_contract($id);

    foreach ($targets as $target) {

      $target_object = new stdClass();

      $target_object->contract_id = $object->contract_id;
      $target_object->target_id = $target['target_id'];
      $target_object->target_type = $target['target_type'];

      drupal_write_record(TARGET_TABLE, $target_object);
    }
  }
}

/**
 * Get the contract(s) from the database which are associated with the given
 * source.
 *
 * @param $source_type
 *  The type of source being queried against.  For example: 'product'.
 * @param $source_id
 *  A sting identifier for the source.  Commonly a node id, but it doesn't
 *  have to be necessarily.
 * @param (optional) $return_all
 *  If passed a paramater of TRUE, this will return all contracts associated
 *  with the source.  Otherwise, it will only return the latest contract
 *  by default.
 *
 * @return
 *  Returns an array containing data about the contract(s) associated with this
 *  source.  If getting multiple values back, contracts will be ordered
 *  in sub-arrays by thier auto-incrementing primary key (most recent on top,
 *  oldest on bottom).
 *
 *  Will return FALSE if nothing was found.
 */
function smart_contracts_get_contracts_for_source($source_type, $source_id,
  $return_all = FALSE) {

  $sql = "
    SELECT *
    FROM {". CONTRACT_TABLE ."} AS ec
    WHERE ec.source_type = '%s'
     AND ec.source_id = '%s'
    ORDER BY ec.contract_id DESC";

  $result = db_query($sql, $source_type, $source_id);

  // If we're going to return all, prepare a return array, populate it and
  // return it
  if ($return_all) {

    // Return array initilization
    $return = array();

    // Loop through and add each record to the return array
    while ($record = db_fetch_array($result)) {

      $return[$record['contract_id']] = $record;
    }

    // If there were no results we'll return FALSE
    if (empty($return)) {

      return FALSE;
    }

    // Return results to caller
    return $return;
  }

  // This is what happens in the default scenario.  We'll return one row,
  // the most recent.
  else {

    // This should return FALSE if there was no record, otherwise it will
    // return the desired record in an array.
    return db_fetch_array($result);
  }
}

/**
 * Return the most recent contract row for a given source
 *
 * @param $source_type
 *  The machine name of the given source type
 * @param $source_id
 *  The identifier for the source (usually NID)
 *
 * @return
 *   Returns the most recent contract record
 */
function smart_contracts_get_most_recent_contract_for_source($source_type, $source_id) {

  $sql = "
    SELECT *
    FROM {". CONTRACT_TABLE ."} AS ec
    WHERE ec.source_type = '%s'
     AND ec.source_id = '%s'";

  $result = db_query($sql, $source_type, $source_id);

  $return = array();

  $result_set = array();
  while ($record = db_fetch_array($result)) {

    $result_set[] = $record;
  }

  if (empty($result_set)) {

    return FALSE;
  }

  $max_vid = 0;
  $most_recent = array();
  foreach ($result_set as $r) {

    if ($r['contract_vid'] > $max_vid) {

      $most_recent = $r;
    }
  }

  return $most_recent;
}

/**
 * Collect all signatures for contracts which have expired past expiry date
 *
 * @return
 *  An array of signature records which are out of date.
 */
function smart_contracts_get_expired_signatures() {

  // Pick up all of the contract_ids from the signature table
  $sql = "
    SELECT DISTINCT(contract_id)
    FROM {". SIGNATURE_TABLE ."}";

  $result = db_query($sql);

  $resultset = array();
  
  // Collect results of query into $resultset
  while ($record = db_fetch_array($result)) {

    $resultset[] = $record;
  }

  // Cycle through $resultset
  foreach ($resultset as $key => $value) {

    // Pick up all of the users who have signed this particular contract
    $sql = "
      SELECT DISTINCT(user_id)
      FROM {". SIGNATURE_TABLE ."}
      WHERE contract_id = %d";
   
    $result = db_query($sql, $resultset[$key]['contract_id']);

    $userset = array();

    while ($record = db_fetch_array($result)) {

      $userset[] = $record;
    }

    // Figure out when this user signed this contract
    foreach ($userset as $userkey => $uservalue) {

      $sql = "
        SELECT MAX(signature_id)
        FROM {". SIGNATURE_TABLE ."}
        WHERE contract_id = %d
        AND user_id = %d";

      $cid_result = db_query($sql, $resultset[$key]['contract_id'],
        $userset[$userkey]['user_id']);

      // If the signature is out of date
      while ($cid_record = db_fetch_array($cid_result)) {

        $signature = _smart_contracts_get_contract_signature($cid_record['MAX(signature_id)']);
        if ($signature['expiry_date'] < time()) {

          // If the signature is out of date, we'll add it to our return array
          $return[] = $signature;
        }
      }
    }
  }

  // There is still more work to do, we must now filter out undesirable results
  $combos = array();

  foreach ($return as $candidate) {

    // Collect contract data and consolidate it with signature data
    $contract = smart_contracts_get_entity_contract($candidate['contract_id']);
    
    $combos[$candidate['signature_id']]['cid'] = $candidate['contract_id'];
    $combos[$candidate['signature_id']]['nid'] = $contract['source_id'];
    $combos[$candidate['signature_id']]['uid'] = $candidate['user_id'];
    $combos[$candidate['signature_id']]['sid'] = $candidate['signature_id'];
  }

  // Duplicate the $combos array
  $subcombos = $combos;

  // Cycle through now and filter out redundant results (we only want the
  // most recent)
  foreach ($combos as $combo) {

    foreach ($subcombos as $key => $subcombo) {

     if ($combo['uid'] == $subcombo['uid'] &&
      $combo['nid'] == $subcombo['nid'] &&
      $combo['cid'] > $subcombo['cid']) {

        unset($subcombos[$key]);
      }
    }
  }

  foreach ($return as $key => $r) {
    if (!array_key_exists($r['signature_id'], $subcombos)) {

      unset($return[$key]);
    }
  }
  return $return;
}

/**
 * Pull in metadata information about a given signature
 *
 * @param $signature_id
 *  The id of the signature in the database
 *
 * @return
 *  Returns signature metadata
 */
function smart_contracts_get_signature_metadata($signature_id) {

  $signature = _smart_contracts_get_contract_signature($signature_id);

  return unserialize($signature['signature_data']);
}

/**
 * Get sigature data from a contract signature
 *
 * @param $signature_id
 *  The primary key for this signature
 *
 * @return
 *  Returns the signature record in question, in array form
 */
function _smart_contracts_get_contract_signature($signature_id) {

  $sql = "
    SELECT *
    FROM {". SIGNATURE_TABLE ."}
    WHERE signature_id = %s";

  $result = db_query($sql, $signature_id);

  while ($record = db_fetch_array($result)) {

    return $record;
  }

  return FALSE;
}

/**
 * Returns the most recent signature for a given contract by the given user
 *
 * @param $uid
 *  The UID of the given user
 * @param $contract_id
 *  The primary key for the contract in the database
 *
 * @return
 *  Returns the most recent signature ofr a given contract by the user.
 */
function smart_contracts_seek_contract_signature($uid, $contract_id,
  $contract_vid = NULL) {

  $sql = "
   SELECT *
   FROM {". SIGNATURE_TABLE ."}
   WHERE user_id = %d
   AND contract_id = %d";

  if ($contract_vid) {

    $sql .= "
      AND current_contract_vid = %d";

    $result = db_query($sql, $uid, $contract_id, $contract_vid);
  }
  else {

    $result = db_query($sql, $uid, $contract_id);
  }

  $return = array();
  $first_pass = TRUE;

  while ($record = db_fetch_array($result)) {

    if ($first_pass) {

      $return = $record;
    }

    if ($record['date_signed'] > $return['date_signed']) {

      $return = $record;
    }
  }

  if (empty($return)) {

    return FALSE;
  }

  return $return;
}

/**
 * Get all signatures submitted by the user
 *
 * @param $uid
 *  The UID of the user in question
 *
 * @return
 *  Returns an array of signature records by the user in question
 */
function smart_contracts_get_signatures_by_user($uid) {

  $sql = "
    SELECT *
    FROM {". SIGNATURE_TABLE ."}
    WHERE uid = %d";

  $result = db_query($sql, $uid);

  $return = array();

  while ($record = db_fetch_array($result)) {

    $return[$record['contract_id']] = $record;
  }

  if (empty($return)) {

    return FALSE;
  }

  return $return;
}

/**
 * Reports on whether or not a given user signed a particular contract 
 * instance
 *
 * @param $uid
 *  The UID of the user in question
 * @param $cid
 *  The contract_id of the contract record in question
 *
 * @return
 *  TRUE if version is signed, FALSE if not.
 */
function smart_contracts_did_user_sign_this_contract_version($uid, $cid) {

  $sql = "
    SELECT *
    FROM {". SIGNATURE_TABLE ."}
    WHERE user_id = %d
    AND contract_id = %d";

  $result = db_query($sql, $uid, $cid);

  if ($record = db_fetch_array($result)) {

    return TRUE;
  }

  return FALSE;
}

/**
 * Reports on whether or not a given user signed any version of the contract
 * past or present
 *
 * @param $uid
 *  The UID of the user in question
 * @param $cid
 *  The contract_id of the contract record in question
 */
function smart_contracts_did_user_sign_this_contract($uid, $cid) {

  $sql = "
    SELECT *
    FROM {". CONTRACT_TABLE ."}
    WHERE contract_id = %d";

  $result = db_query($sql, $cid);

  $contract_versions = array();

  if ($record = db_fetch_array($result)) {

    $source_id = $record['source_id'];
    $contract_nid = $record['contract_nid'];
  }
  else {

    return NULL;
  }

  $sql_l2 = "
    SELECT contract_id
    FROM {". CONTRACT_TABLE ."}
    WHERE source_id = '%s'
    AND contract_nid = %d";

  $result_l2 = db_query($sql_l2, $source_id, $contract_nid);

  $contracts_in_series = array();
  $substitution_string = array();

  $query_input[] = $uid;
  while ($record_l2 = db_fetch_array($result_l2)) {

    $query_input[] = $record_l2['contract_id'];
    $substitution_string[] = '%d';
  }

  $sql_l3 = "
    SELECT *
    FROM {". SIGNATURE_TABLE ."}
    WHERE user_id = %d
    AND contract_id IN (";

  $sql_l3 .= implode(',', $substitution_string);

  $sql_l3 .= ")";

  $result_l3 = db_query($sql_l3, $query_input);

  if ($record_l3 = db_fetch_array($result_l3)) {

    return TRUE;
  }

  return FALSE;
}

/**
 * Identifies whether or not a new entry should be made for a contract after
 * a user has changed the settings on a given Contract / Source relationship.
 *
 * @param $propsed_contract
 *  Contract data reflecting the settings requested by the user
 * @param $current_contract
 *  Contract data refelecting the crrent state of the contract in question
 *
 * @return
 *  Return TRUE if contract should be given a new line, FALSE if not
 */
function _smart_contracts_needs_new_entry($proposed_contract,
  $current_contract) {

  $needs_new_entry = FALSE;  // Return flag to track results of check.

  // Compare the 3 unique keys from the db table.  If any one of them
  // are different, this will prompt us to create a new contract row
  // (while still keeping the old for our records).
  if ($proposed_contract['source_id'] != $current_contract['source_id']) {

    $needs_new_entry = TRUE;
  }

  if ($proposed_contract['contract_nid'] != $current_contract['contract_nid']) {

    $needs_new_entry = TRUE;
  }

  if ($proposed_contract['contract_vid'] != $current_contract['contract_vid']) {

    $needs_new_entry = TRUE;
  }

  return $needs_new_entry;
}

/**
 * Identifies whether or not a contract record ought to be updated because
 * of changes to secondary data (such as signature requirement options)
 *
 * @param $proposed_contract
 * Contract data reflecting the settings requested by the user
 * @param $current_contract
 *  Contract data reflecting the curreing state of the contract in question
 *
 * @return
 *  Return TRUE if contract should be updated, FALSE if not
 */
function _smart_contracts_needs_update($proposed_contract, $current_contract) {

  $needs_update = FALSE; // Return flag to track results of check

  // Compare all secondary data (contract options) which may be switched
  // by the user.

  if ($proposed_contract['expiration_date'] !=
    $current_contract['expiration_date']) {

    $needs_update = TRUE;
  }

  if ((bool) $proposed_contract['signature_required'] !=
    (bool) $current_contract['signature_required']) {

    $needs_update = TRUE;
  }

  if ((bool) $proposed_contract['signature_required_on_revision'] !=
    (bool) $current_contract['signature_required_on_revision']) {

    $needs_update = TRUE;
  }

  if ((bool) $proposed_contract['enabled'] !=
      (bool) $current_contract['enabled']) {

    $needs_update = TRUE;
  }

  return $needs_update;
}

/**
 * Insert a new contract row into the core contract database table to
 * represent a critical update to a contract row which already exists
 *
 * @param $proposed_contract
 *  Contract data reflecting the settings requested by the user
 * @param $current_contract
 *  Contract data reflecting the current state of the contract in question
 */
function smart_contracts_insert_alternate_contract_record($proposed_contract,
  $current_contract) {

  // Absorb all unique key changes from the proposed contract, as well as
  // secondary data.
  $current_contract['source_id'] = $proposed_contract['source_id'];
  $current_contract['contract_nid'] = $proposed_contract['contract_id'];
  $current_contract['contract_vid'] = $proposed_contract['contract_vid'];
  $current_contract['signature_required'] =
    $proposed_contract['signature_required'];
  $current_contract['signature_required_on_revision'] =
    $proposed_contract['signature_required_on_revision'];
  $current_contract['expiration_date'] = $proposed_contract['expiration_date'];
  $current_contract['implementation_date'] = time();

  // Save modified 'current' contract as a new contract row in the DB.
  $success = drupal_write_record(CONTRACT_TABLE, $current_contract);

  if (!$success) {

    // This absolutely should have succeeded.  If not, there was definitely
    // a major error.
    throw new Exception('Failed to write alternate contract record', 5001);
  }
}

/**
 * Update an existing contract entry with changes to secondary data, such
 * as contract options.
 *
 * @param $proposed_contract
 *  Contract data reflecting the settings requested by the user
 * @param $current_contract
 *  Contract data reflecting the current state of the contract in question
 */
function smart_contracts_update_contract_entry($proposed_contract,
  $current_contract) {

  // Absorb changed settings from propsed contract.
  $current_contract['signature_required'] =
    ($proposed_contract['signature_required']) ? 1 : 0;
  $current_contract['signature_required_on_revision'] =
    ($proposed_contract['signature_required_on_revision']) ? 1 : 0;
  $current_contract['expiration_date'] = $proposed_contract['expiration_date'];
  $current_contract['enabled'] = ($proposed_contract['enabled']) ? 1 : 0;

  // Write an UPDATE to the database to update the option status of this
  // contract
  $success = drupal_write_record(CONTRACT_TABLE, $current_contract,
    array('contract_id'));

  if (!$success) {

    // This absolutely should have succeeded.  If not, there was definitely
    // a major error.
    throw new Exception('Failed to update contract record', 5002);
  }
}

/**
 * Inserts a brand spanking new contract entry row in the core contract
 * database.  Used only when assigning contracts / targets to NODE sources.
 *
 * @param $values
 *  The $values var is used to determine the settings for the contract.
 *  These values are modeled after the $form_state['values'] array
 *  which would come from a node_edit form upon submit, using the
 *  smart_contracts_form form.
 * @param $contract_node
 *  The node which contains the contract that the user wishes to tie to
 *  the provided source type.
 * @param $source_node
 *  The node which will be bound to the given contract
 */
function smart_contracts_node_source_insert_new_contract_record(
  $values, $contract_node, $source_node) {

  $a = 1;

  $new_record = new stdClass(); // Container for new DB row

  // Pull out all variables explicitly (this is done to make debugging easier)
  $source_id           = $source_node->nid;
  $source_type         = $source_node->type;
  $contract_nid        = $contract_node->nid;
  $contract_vid        = $contract_node->vid;
  $implementation_date = time();
  $expiration_date     = $values['contract_expiration_date'];
  $signature_required  =
    ($values['signature_required_once']) ? 1 : 0;
  $signature_required_on_revision =
    ($values['signature_required_always']) ? 1 : 0;
  $enabled             = 1;  // We should never get here if enabled is off
  
  // Now apply all explicit variables to our db object
  $new_record->source_id           = $source_id;
  $new_record->source_type         = $source_type;
  $new_record->contract_nid        = $contract_nid;
  $new_record->contract_vid        = $contract_vid;
  $new_record->implementation_date = $implementation_date;
  $new_record->expiration_date     = $expiration_date;
  $new_record->signature_required  = $signature_required;
  $new_record->signature_required_on_revision =
    $signature_required_on_revision;
  $new_record->enabled             = $enabled;

  // Define the name of the table we are writing to
  $target_table = CONTRACT_TABLE;

  // Commit new record to database
  $success = drupal_write_record($target_table, $new_record);

  if (!$success) {

    // This absolutely should have succeeded.  If not, there was definitely
    // a major error.
    throw new Exception('Failed to create new contract record', 5001);
  }
}

/**
 * Sets the given contract to 'disabled' in the database
 *
 * @param $contract
 *  The contract which will be disabled.  Should be a db object.
 */
function smart_contracts_disable_contract($contract) {

  if (!$contract) {

    throw new Exception('$contract variable missing in smart_contracts_disable_contracts routine', 5003);
  }
  // Ensure the $contract is set to disabled
  $contract['enabled'] = 0;

  // Declare the target table
  $target_table = CONTRACT_TABLE;

  // Commit the change to the database row
  $success = drupal_write_record($target_table, $contract, array('contract_id'));

  if (!$success) {

    // This absolutely should have succeeded.  If not, there was definitely
    // a major error.
    throw new Exception('Failed to disable contract record', 5002);
  }
}